"""
Jotunn Engine Model exporter addon for Blender
"""

import bpy
import bmesh
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
import struct

bl_info = {
    "name": "JEM Exporter",
    "author": "Arnau Bigas",
    "version": (0, 0, 0),
    "blender": (2, 78, 0),
    "location": "File > Export > JE Model (.jem)",
    "description": "Model exporter for the Jotunn Engine",
    "category": "Import-Export"
}

class JEMExporter(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.jem_exporter"
    bl_label = "JEMExporter"
    bl_option = {'PRESET'}
    filename_ext = ".jem"

    obj_name = ""

    def __init__(self):
        pass

    def execute(self, context):
        f = open(self.filepath, 'wb')

        self.writeHeader(f) #write header

        f.write(struct.pack('B', len(bpy.data.materials)))
        for mat in bpy.data.materials:
            self.writeMaterial(f, mat)

        f.write(struct.pack('B', len(bpy.context.selected_objects)))
        for obj in bpy.context.selected_objects:
            self.writeObj(f, obj)

        f.close()
        return {'FINISHED'}

    def writeHeader(self, f):
        #magic + version
        f.write(struct.pack('3sB', "JEM".encode('UTF-8'), 0))

        #pipeline name
        f.write(struct.pack('5s', "test".encode('UTF-8')))

        #texture info
        f.write(struct.pack('B', 0)) #temp, no texture info

        #physics info (mass, volume, centreOfMass)
        f.write(struct.pack('5f', 0, 0, 0, 0, 0)) #temp, mass = volume = 0

    def writeMaterial(self, f, mat):
        #diffuse info
        f.write(struct.pack('3f', mat.diffuse_color.r, mat.diffuse_color.g,
            mat.diffuse_color.b))

        #specularity info
        f.write(struct.pack('4fH', mat.specular_color.r, mat.specular_color.g,
            mat.specular_color.b, mat.specular_intensity,
            mat.specular_hardness))

        #emissivity
        f.write(struct.pack('f', mat.emit))


    def writeObj(self, f, obj):
        #available data
        f.write(struct.pack('B', 0))

        mesh = obj.to_mesh(bpy.context.scene, True, 'RENDER')

        bm = bmesh.new()
        bm.from_mesh(mesh)
        bmesh.ops.triangulate(bm, faces=bm.faces)
        bm.to_mesh(mesh)
        bm.free()

        mesh.calc_normals_split()
        mesh.calc_tessface()

        #position data
        f.write(struct.pack('I', len(mesh.vertices)))
        print("positions: ", len(mesh.vertices))
        for vert in mesh.vertices:
            f.write(struct.pack('3f', vert.co.x, vert.co.z, vert.co.y)) #z<->y

        #calculate index data and generate normals
        normals = {}
        vertices = {}
        indices = []
        normalCount = 0
        vertexCount = 0
        indexCount = 0
        for face in mesh.tessfaces:
            for i in range(0, 3):
                normal = face.split_normals[i]
                n = normal[0], normal[2], normal[1] #z<->y
                if n not in normals:
                    val = normals[n] = normalCount
                    normalCount += 1
                else:
                    val = normals[n]
                v = (face.vertices[i], val)
                if v not in vertices:
                    val = vertices[v] = vertexCount
                    vertexCount += 1
                else:
                    val = vertices[v]
                indices.append(val)

        #write normals
        f.write(struct.pack('I', len(normals)))
        print("normals: ", len(normals))
        for normal in normals:
            f.write(struct.pack('3f', normal[0], normal[1], normal[2]))

        #write indexed vertices
        f.write(struct.pack('I', len(vertices)))
        print("data indices: ", len(vertices))
        for vertex in vertices:
            f.write(struct.pack('2I', vertex[0], vertex[1]))

        #write indices
        f.write(struct.pack('I', len(indices)))
        print("indices: ", len(indices))
        for index in indices:
            f.write(struct.pack('I', index))

        bpy.data.meshes.remove(mesh)

def createMenu(self, context):
    self.layout.operator(JEMExporter.bl_idname, text="JE Model (.jem)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(createMenu)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(createMenu)

# load the addon even from the text editor
if __name__ == "__main__":
    register();
