JEM Export
=============

This is a python addon for the 3D modeling software "Blender" that adds the
option to export models in the ".jem" (Jotunn Engine Model) binary format.
